#pragma once

 /* __  __            _                      _ */   
/* |  \/  |_   _     / \   ___ ___  ___ _ __| |_ */ 
/* | |\/| | | | |   / _ \ / __/ __|/ _ \ '__| __| */
/* | |  | | |_| |  / ___ \\__ \__ \  __/ |  | |_ */ 
/* |_|  |_|\__, | /_/   \_\___/___/\___|_|   \__| */
 /*        |___/ */                                 
/* This is a header I can use in many places. */

#include <features.h>

#ifndef NDEBUG
/* Let's have fun with compiler-specific debug commands! */
#ifdef _MSC_VER /* TODO: Make this specific to a version of MSC, and confirm it works. */
#define MY_ASSERT(X) {\
    if (!(X)) __debug_break();\
}

#elif defined(_POSIX_SOURCE)
#include <signal.h>
/* The signal meant for debugging!!! */
#define MY_ASSERT(X) {\
    if (!(X)) raise(SIGTRAP);\
}

#else /* Boring but sensible default. */
#include <assert.h> /* Only needed in this base case; save an include! */

#define MY_ASSERT(X) assert(X)
#endif

#else /* NDEBUG defined. */
#define MY_ASSERT(X) ((void) 0)
#endif
// vim: ft=c
