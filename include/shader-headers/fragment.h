#pragma once
const GLchar * fragment_shader_text = 
"#version 330 core\n"
"out vec4 FragColor;\n"
"\n"
"uniform vec3 ourPos;\n"
"\n"
"in vec3 ourColour;\n"
"in vec4 pixPos;\n"
"\n"
"void main()\n"
"{\n"
"    FragColor = vec4((ourColour+ourPos+pixPos.xyz)/3, 1.0);\n"
"}\n"
"\0";
const size_t fragment_shader_text_len = 174;
