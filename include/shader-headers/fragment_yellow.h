#pragma once
const GLchar * fragment_yellow_shader_text = 
"#version 330 core\n"
"out vec4 FragColor;\n"
"\n"
"void main()\n"
"{\n"
"    FragColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);\n"
"}\n"
"\n"
"\0";
const size_t fragment_yellow_shader_text_len = 103;
