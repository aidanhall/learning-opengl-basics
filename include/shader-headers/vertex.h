#pragma once
const GLchar * vertex_shader_text = 
"#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"layout (location = 1) in vec3 aColour;\n"
"\n"
"out vec3 ourColour;\n"
"out vec4 pixPos;\n"
"\n"
"uniform float time_value;\n"
"\n"
"void main()\n"
"{\n"
"    vec2 oscillation = vec2( sin(time_value + (aPos.y * 2 * 3.1415)) , sin( time_value + (aPos.x * 2 * 3.1415) )) * 0.0625;\n"
"    gl_Position = pixPos = vec4(aPos.xy+oscillation, aPos.z, 1.0);\n"
"    ourColour = aColour;\n"
"    /* vec4(0.5, 0.0, 0.0, 1.0); */\n"
"}\n"
"\0";
const size_t vertex_shader_text_len = 428;
