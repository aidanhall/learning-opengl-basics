#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "glad/glad.h"
#include "GLFW/glfw3.h"

#define SC_WIDTH 800
#define SC_HEIGHT 600
#define SIN_SPACED(N) (2.0f*M_PI/(float)N)

#define LENGTH(X) ((sizeof(X))/(sizeof(X[0])))

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void process_input(GLFWwindow* window);

#include "my_assert.h"

#ifndef NDEBUG
#define GL_CALL_UNSCOPED(FUNC) /* Unscoped should not be default, but available. */\
    gl_clear_errors();\
    FUNC;\
    MY_ASSERT(gl_log_call(__LINE__, #FUNC, __FILE__));

#else
#define GL_CALL_UNSCOPED(FUNC) FUNC

#endif


#define GL_CALL(FUNC) {\
    GL_CALL_UNSCOPED(FUNC)\
}

#define ERROR_FMT_STR "%s:%s:%d:%x:"
bool gl_log_call(const uint32_t line, const char* function, const char* file);
void gl_clear_errors();
void gl_print_error(const uint32_t line, const char* function, const char* file, GLenum code);

/* The array of shader IDs is heap-allocated. */
uint32_t *gl_compile_shaders(const GLenum *shader_types, const GLchar **shader_sources, uint32_t n_shaders);
uint32_t gl_create_shader_program(uint32_t *shaders, uint32_t n_shaders);

// vim: ft=c
