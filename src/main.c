#include "main.h"

#include "shader-headers/vertex.h"
#include "shader-headers/fragment.h"
/* #include "shader-headers/fragment_yellow.h" */

const uint_fast8_t n_vertex_attributes = 6;
float vertices[] = {
	/*  X,	    Y,	  Z,	R,	G,  B */
	0.0f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
	0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
	-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
	-0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 1.0f,
	-0.9f, -0.9f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.9f, -0.9f, 0.0f, 0.0f, 1.0f, 1.0f,
	-0.2f, 0.2f, 0.0f, 0.0f, 0.0f, 0.0f,
	0.8f, 0.0f, 0.0f,0.0f,0.0f,0.0f,

};

uint32_t indices[] = {
	0,1,3,
	7,6,2,
	2,5,4,
};

int main(int argc, char* argv[]) {
	// GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(SC_WIDTH, SC_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to create GLFW window.\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// VIEWPORT
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	// GLAD
	if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		fprintf(stderr, "Failed to initialise GLAD.\n");
		return -1;
	}
	// GLEW
	/* if(glewInit() != GLEW_OK) { */
	/* 	fprintf(stderr, "Failed to initialise GLEW.\n"); */
	/* 	return -1; */
	/* } */

	uint32_t shader_program;
	/* uint32_t shader_program_2; */

	{ // Shaders and shader program.
		// Shader program 1.
		const GLchar* shader_sources[] = {vertex_shader_text, fragment_shader_text};
		const GLenum shader_types[] = {GL_VERTEX_SHADER, GL_FRAGMENT_SHADER};
		uint32_t* shaders = gl_compile_shaders(shader_types, shader_sources, LENGTH(shader_sources));

		shader_program = gl_create_shader_program(shaders, LENGTH(shader_sources));
		free(shaders);
	}

	// Generate VAO, VBO and EBO.
	uint32_t VAO;
	uint32_t VBO;
	uint32_t EBO;
	// Generate Vertex Array Object: It stores the current VBO 'configuration' for later.
	GL_CALL(glGenVertexArrays(1, &VAO));
	// Generate Vertex Buffer Object.
	GL_CALL(glGenBuffers(1, &VBO));
	// Element Buffer Object.
	GL_CALL(glGenBuffers(1, &EBO));


	GL_CALL(glBindVertexArray(VAO));

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, VBO));
	GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW));

	GL_CALL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO));
	GL_CALL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW));

	// Set vertex attribute pointers.
	// Linking Vertex Attributes.
	// Position
	GL_CALL(glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, n_vertex_attributes*sizeof(float), (void*)0));
	GL_CALL(glEnableVertexAttribArray(0));
	// Colour
	GL_CALL(glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, n_vertex_attributes*sizeof(float),
				(void*)(3*sizeof(float))));
	GL_CALL(glEnableVertexAttribArray(1));


	// Unbind VBO and VAO for tidiness.
	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, 0));
	GL_CALL(glBindVertexArray(0));

	/* GL_CALL(glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)); */

	/* float h_offset = 0.0f; */
	/* GL_CALL(glUseProgram(shader_program)); */
	/* GL_CALL(glUniform1f(glGetUniformLocation(shader_program, "hoffset"), h_offset)); */
	/* GL_CALL(glUseProgram(0)); */

	// RENDER LOOP
	while (!glfwWindowShouldClose(window)) {
		process_input(window);
		// Clearing colour.
		GL_CALL(glClearColor(0.2f, 0.3f, 0.3f, 1.0f));
		GL_CALL(glClear(GL_COLOR_BUFFER_BIT));

		GL_CALL(glUseProgram(shader_program));

		{
			// Colour modulation.
			float time_value = glfwGetTime();
			float green_value = (sinf(1.0f*time_value) / 2.0f) + 0.5f;
			float red_value = (sinf(1.0f * (time_value + SIN_SPACED(3.0f))) / 2.0f) + 0.5f;
			float blue_value = (sinf(1.0f*(time_value - SIN_SPACED(3.0f))) / 2.0f) + 0.5f;
			int vertex_colour_location;
			GL_CALL(vertex_colour_location = glGetUniformLocation(shader_program, "ourPos"));

			GL_CALL(glUniform3f(vertex_colour_location, red_value, green_value, blue_value));

			GL_CALL(glUseProgram(shader_program));
			GL_CALL(glUniform1f(glGetUniformLocation(shader_program, "time_value"), time_value));
		}

		// Bind (load) the VBO attributes from the VAO.
		GL_CALL(glBindVertexArray(VAO));

		GL_CALL(glDrawElements(GL_TRIANGLES, LENGTH(indices), GL_UNSIGNED_INT, 0));

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// Deallocate.
	GL_CALL(glDeleteVertexArrays(1, &VAO));
	GL_CALL(glDeleteBuffers(1, &VBO));
	GL_CALL(glDeleteBuffers(1, &EBO));
	GL_CALL(glDeleteProgram(shader_program));

	// Stop glfw.
	glfwTerminate();
	return 0;
}


void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	GL_CALL(glViewport(0, 0, width, height));
}

void process_input(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		GL_CALL(glPolygonMode(GL_FRONT_AND_BACK, GL_LINE));

	if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
		GL_CALL(glPolygonMode(GL_FRONT_AND_BACK, GL_FILL));

}

uint32_t *gl_compile_shaders(const GLenum *shader_types, const GLchar **shader_sources, uint32_t n_shaders) {
	uint32_t *shaders = calloc(n_shaders, sizeof(uint32_t));
	for (int i = 0; i < n_shaders; ++i){
		GL_CALL(shaders[i] = glCreateShader(shader_types[i]));
		GL_CALL(glShaderSource(shaders[i], 1, &shader_sources[i], NULL));
		GL_CALL(glCompileShader(shaders[i]));

		{ // Check compilation.
			int success;
			char infoLog[512];
			GL_CALL(glGetShaderiv(shaders[i], GL_COMPILE_STATUS, &success));
			if (!success) {
				GL_CALL(glGetShaderInfoLog(shaders[i], 512, NULL, infoLog));
				fprintf(stderr, "Shader didn't compile: %s\n", infoLog);
			}
		}
	}
	return shaders;
}

uint32_t gl_create_shader_program(uint32_t *shaders, uint32_t n_shaders) {
	uint32_t shader_program;
	GL_CALL(shader_program = glCreateProgram());

	for (int i = 0; i < n_shaders; ++i) {
		GL_CALL(glAttachShader(shader_program, shaders[i]));
		GL_CALL(glDeleteShader(shaders[i]));
	}
	GL_CALL(glLinkProgram(shader_program));

	{ // Check compilation.
		int success;
		char infoLog[512];
		GL_CALL(glGetProgramiv(shader_program, GL_LINK_STATUS, &success));
		if (!success) {
			GL_CALL(glGetShaderInfoLog(shader_program, 512, NULL, infoLog));
			fprintf(stderr, "Shader program didn't compile: %s\n", infoLog);
			return 0;
		}
	}
	return shader_program;
}

bool gl_log_call(const uint32_t line, const char* function, const char* file) { // Inspired by The Cherno. :)
	GLenum error;
	while ((error = glGetError()) != GL_NO_ERROR) {
		gl_print_error(line, function, file, error);
		return false;
	}

	return true;
}

void gl_clear_errors() {
	while (glGetError() != GL_NO_ERROR);
}

void gl_print_error(const uint32_t line, const char* function, const char* file, GLenum code) {
	fprintf(stderr, ERROR_FMT_STR, file, function, line, code);
	switch (code) {
		case  0:
			break;
		case  0x0500:
			fputs("GL_INVALID_ENUM\n", stderr);
			break;
		case  0x0501:
			fputs("GL_INVALID_VALUE\n", stderr);
			break;
		case  0x0502:
			fputs("GL_INVALID_OPERATION\n", stderr);
			break;
		case  0x0505:
			fputs("GL_OUT_OF_MEMORY\n", stderr);
			break;
		default:
			fputs("UNDEFINED_ERROR\n", stderr);
			break;
	}
}
