#version 330 core
out vec4 FragColor;

uniform vec3 ourPos;

in vec3 ourColour;
in vec4 pixPos;

void main()
{
    FragColor = vec4((ourColour+ourPos+pixPos.xyz)/3, 1.0);
}
