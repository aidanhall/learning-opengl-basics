#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColour;

out vec3 ourColour;
out vec4 pixPos;

uniform float time_value;

void main()
{
    vec2 oscillation = vec2( sin(time_value + (aPos.y * 2 * 3.1415)) , sin( time_value + (aPos.x * 2 * 3.1415) )) * 0.0625;
    gl_Position = pixPos = vec4(aPos.xy+oscillation, aPos.z, 1.0);
    ourColour = aColour;
    /* vec4(0.5, 0.0, 0.0, 1.0); */
}
